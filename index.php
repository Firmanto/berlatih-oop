<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $sheep = new Animal("shaun");

    echo "nama : ".$sheep->name."<br>"; // "shaun"
    echo "jumlah kaki : ".$sheep->legs."<br>"; // 4
    echo "berdarah dingin :  ".$sheep->cold_blooded."<br>"."<br>"; // "no"
    
    $kodok = new Frog("buduk");
    echo "nama : ".$kodok->name."<br>"; 
    echo "jumlah kaki : ".$kodok->legs."<br>";
    echo "berdarah dingin : ".$kodok->cold_blooded."<br>"; 
    $kodok->jump(); // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "nama : ".$sungokong->name."<br>"; 
    echo "jumlah kaki : ".$sungokong->legs."<br>"; 
    echo "berdarah dingin : ".$sungokong->cold_blooded."<br>"; 
    $sungokong->yell() // "Auooo"

    
?>